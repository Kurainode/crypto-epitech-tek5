import urllib.request
import os
import sys
import subprocess

def open_image(path):
    imageViewerFromCommandLine = {'linux':'xdg-open', 'win32':'explorer', 'darwin':'open'}[sys.platform]
    subprocess.run([imageViewerFromCommandLine, path])

def xor_file(file, key):
    l = len(key)
    file_bytes = bytearray(open(file, 'rb').read())

    for i in range(len(file_bytes)):
            file_bytes[i] = file_bytes[i] ^ key[i % l]

    print(file_bytes[:10])
    os.remove(file)
    open(file, 'wb').write(file_bytes)
    open_image(file)

to_decrypt = urllib.request.urlretrieve("https://crypto.blackfoot.io/ch2.bmp", "ch2.bmp")

xor_file("ch2.bmp", b"gitgud")