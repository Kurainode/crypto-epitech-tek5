import urllib.request
import os
import sys
import subprocess
import ctypes

def open_image(path):
    imageViewerFromCommandLine = {'linux':'xdg-open', 'win32':'explorer', 'darwin':'open'}[sys.platform]
    subprocess.run([imageViewerFromCommandLine, path])

def cbc_file(file, key):
    l = len(key)
    file_bytes = bytearray(open(file, 'rb').read())
    #print((len(file_bytes) - 8).to_bytes(4, 'little'))


    for i in range(len(file_bytes)):
            file_bytes[i] = file_bytes[i] ^ key[i % l]
            #key[i % l] = file_bytes[i]

    print(file_bytes[:16])
    os.remove(file)
    open(file, 'wb').write(file_bytes)
    #open_image(file)

to_decrypt = urllib.request.urlretrieve("https://crypto.blackfoot.io/basic4.webp", "basic4.webp")

cbc_file("basic4.webp", bytearray(b"w?v=MsQaPR2NjP8"))