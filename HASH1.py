import hashlib

def crack_hash(hash, max_lengh, current):
    dictionary = bytearray(b"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")
    current_len = len(current)
    print(current)

    if current_len > max_lengh:
        return ""

    current.append(dictionary[0])

    for i in range(len(dictionary)):
        current[current_len] = dictionary[i]

        if hashlib.md5(current).digest() == hash:
            return current

        recurs = crack_hash(hash, max_lengh, bytearray(current))
        if recurs != "":
            return recurs

    return ""


print(crack_hash(b"37f62f1363b04df4370753037853fe88", 7, bytearray(b'')).encode())

