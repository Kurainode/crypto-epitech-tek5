import urllib.request
to_decrypt = str(urllib.request.urlopen("https://crypto.blackfoot.io").read().decode('ascii'))

def substitution_decrypt(key, text):
    alpha = "abcdefghijklmnopqrstuvwxyz"
    dictionary = {}
    decrypted = ''

    for i in range(len(alpha)):
        dictionary[key[i]] = alpha[i]
    
    for i in range(len(alpha)):
        dictionary[key[i].upper()] = alpha[i].upper()
    
    for i in range(len(to_decrypt)):
        if to_decrypt[i].isalpha():
            decrypted += dictionary[to_decrypt[i]]
        else:
            decrypted += to_decrypt[i]

    return decrypted

print(substitution_decrypt("dpejrflmsnkihuxzoqtvwcbayg", to_decrypt))