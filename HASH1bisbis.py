import hashlib

def crack_hash(hash, max_lengh, current):
    dictionary = bytearray(b"0123456789")
    current_len = len(current)
    print(current)

    if current_len > max_lengh:
        return ""

    current.append(dictionary[0])

    for i in range(len(dictionary)):
        current[current_len] = dictionary[i]

        if hashlib.md5(current).digest() == hash:
            return current

        recurs = crack_hash(hash, max_lengh, bytearray(current))
        if recurs != "":
            return recurs

    return ""


print(crack_hash(b"45fd94189e37d05e221becb368220c1b", 8, bytearray(b'')).encode())

