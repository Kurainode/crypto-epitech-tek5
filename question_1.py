to_decrypt = 'ctlf.rpobakotiy.coo'

def rail_fence_decrypt(lines, text):
    buffer = [''] * lines
    decrypted = ''
    
    l = len(text)
    off_x = -1
    off_y = lines * 2 - 3
    pos = 0
    for line in range(lines):
        i = 0
        off_switch = False
        while i < l and pos < l:
            if off_x > 0 and (not off_switch or off_y <= 0) :
                if i == 0:
                    buffer[line] += ' ' * (off_x // 2 + 1)
                    i +=  (off_x // 2 + 1)
                else:
                    buffer[line] += ' ' * off_x
                    i += off_x
            
            if off_y > 0 and (off_switch or off_x <= 0) and i > 0:
                if i == 0:
                    buffer[line] += ' ' * (off_y // 2 + 1)
                    i +=  (off_y // 2 + 1)
                else:
                    buffer[line] += ' ' * off_y
                    i += off_y

            if (i < l):
                buffer[line] += to_decrypt[pos]
                pos += 1
                i += 1
            
            off_switch = not off_switch
        off_x += 2
        off_y -= 2
    
    line = 0
    add = True
    for i in range(l):
        decrypted += buffer[line][i]

        if line + 1 == lines:
            add = False
        elif line == 0:
            add = True
        if add:
            line += 1
        else:
            line -= 1
    
    return decrypted

print(rail_fence_decrypt(3, to_decrypt))